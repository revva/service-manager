# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="0.2.0"></a>
# [0.2.0](https://gitlab.com/revva/service-manager/compare/v0.1.1...v0.2.0) (2019-11-04)


### Bug Fixes

* fix return new instance of a singleton service ([#13](https://gitlab.com/revva/service-manager/issues/13)) ([cf71371](https://gitlab.com/revva/service-manager/commit/cf71371))


### Features

* add a method which returns a services map ([#12](https://gitlab.com/revva/service-manager/issues/12)) ([0f542db](https://gitlab.com/revva/service-manager/commit/0f542db))



<a name="0.1.1"></a>

## [0.1.1](https://gitlab.com/revva/service-manager/compare/v0.1.0...v0.1.1) (2019-02-23)

<a name="0.1.0"></a>

# [0.1.0](https://gitlab.com/revva/service-manager/compare/v0.0.4...v0.1.0) (2019-02-23)

<a name="0.0.4"></a>

## [0.0.4](https://gitlab.com/revva/service-manager/compare/v0.0.3...v0.0.4) (2019-02-22)

### Features

-   add declaration type "factory"
    ([#3](https://gitlab.com/revva/service-manager/issues/3))
    ([ef4f3b3](https://gitlab.com/revva/service-manager/commit/ef4f3b3))

<a name="0.0.3"></a>

## [0.0.3](https://gitlab.com/revva/service-manager/compare/v0.0.2...v0.0.3) (2019-02-19)

### Code Refactoring

-   use service manager as class
    ([e23dfce](https://gitlab.com/revva/service-manager/commit/e23dfce))

### BREAKING CHANGES

-   service manager is now created using class constructor

Before:

```ts
const decls = {};
const sm = createServiceManager(decls);
```

After:

```ts
const decls = {};
const sm = new ServiceManager(decls);
```

<a name="0.0.2"></a>

## [0.0.2](https://gitlab.com/revva/service-manager/compare/v0.0.1...v0.0.2) (2019-02-16)

### Features

-   add service declaration
    ([#2](https://gitlab.com/revva/service-manager/issues/2))
    ([6d8a181](https://gitlab.com/revva/service-manager/commit/6d8a181))

<a name="0.0.1"></a>

## 0.0.1 (2019-01-28)

const path = require('path');
const util = require('util');
const exec = util.promisify(require('child_process').exec);
const fs = require('fs-extra');
const packageJson = require('../package');

const ROOT_PATH = path.resolve(__dirname, '..');
const PACKAGE_PATH = path.join(ROOT_PATH, '.package');

const copyFile = async (fileName) => {
  await fs.copy(
    path.join(ROOT_PATH, fileName),
    path.join(PACKAGE_PATH, fileName),
  );
};

const clearPackageDir = async () => {
  await fs.remove(PACKAGE_PATH).then(() => fs.mkdir(PACKAGE_PATH));
};

const createPackageJson = async () => {
  const content = JSON.stringify(
    {
      name: packageJson.name,
      version: packageJson.version,
      description: packageJson.description,
      license: packageJson.license,
      author: packageJson.author,
      homepage: packageJson.homepage,
      repository: packageJson.repository,
      bugs: packageJson.bugs,
      main: './src/index.js',
      types: './src/index.d.ts',
    },
    null,
    2,
  );
  const filePath = path.join(PACKAGE_PATH, 'package.json');
  await fs.writeFile(filePath, content);
};

const createNpmRc = async () => {
  const content = '//registry.npmjs.org/:_authToken=${NPM_TOKEN}';
  const filePath = path.join(PACKAGE_PATH, '.npmrc');
  await fs.writeFile(filePath, content);
};

const compile = async () => {
  try {
    await exec('npx tsc', {cwd: ROOT_PATH});
  } catch (err) {
    throw new Error(err.stdout || err.message);
  }
};

const main = async () => {
  await clearPackageDir();
  await Promise.all([
    compile(),
    createPackageJson(),
    createNpmRc(),
    copyFile('README.md'),
    copyFile('LICENSE'),
  ]);
};

module.exports = main;

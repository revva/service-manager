type ServiceMap<M> = {[K in keyof M]: M[K]};

export interface ServiceDeclaration<S> {
  type: DECLARATION_TYPE.SERVICE;
  service: S | Promise<S>;
}

export interface FactoryDeclaration<
  M extends ServiceMap<M>,
  K extends keyof M
> {
  type: DECLARATION_TYPE.FACTORY;
  factory:
    | ((sm: ServiceManager<M>) => M[K])
    | ((sm: ServiceManager<M>) => Promise<M[K]>);
  isSingleton?: boolean;
}

export type Declaration<M extends ServiceMap<M>, K extends keyof M> =
  | ServiceDeclaration<M[K]>
  | FactoryDeclaration<M, K>;

export type Declarations<M extends ServiceMap<M>> = {
  [K in keyof M]: Declaration<M, K>
};

export enum DECLARATION_TYPE {
  SERVICE = 'service',
  FACTORY = 'factory',
}

export class ServiceManager<M extends ServiceMap<M>> {
  private singletons: {[K in keyof M]?: Promise<M[K]> | M[K]} = {};

  public constructor(readonly declarations: Declarations<M>) {}

  public get = async <K extends keyof M>(serviceName: K): Promise<M[K]> => {
    const declaration: Declaration<M, K> = this.declarations[serviceName];
    if (declaration) {
      switch (declaration.type) {
        case DECLARATION_TYPE.FACTORY:
          return this.factory(serviceName, declaration);
        case DECLARATION_TYPE.SERVICE:
          return this.service(declaration);
        default:
          throw new Error(
            `Invalid declaration type: "${(declaration as any).type}".`,
          );
      }
    }
    throw new Error(`Service with name "${serviceName}" is undeclared.`);
  };

  public getMap = async <T extends {[K in keyof T]: keyof M}>(
    namesMap: T,
  ): Promise<{[K in keyof T]: M[K]}> => {
    type ServiceMap = {[K in keyof T]: M[K]};
    const nameEntries = Object.entries<keyof M>(namesMap);
    const serviceEntries = nameEntries.map<[keyof T, Promise<M[keyof T]>]>(
      ([aliasName, serviceName]) => {
        return [aliasName as keyof T, this.get(serviceName)];
      },
    );
    const servicePromises = serviceEntries.map(([, servicePromise]) => {
      return servicePromise;
    });
    return Promise.all(servicePromises).then((services) => {
      return serviceEntries.reduce<ServiceMap>(
        (serviceMap, [aliasName], i) => {
          serviceMap[aliasName] = services[i];
          return serviceMap;
        },
        {} as ServiceMap,
      );
    });
  };

  private async service<K extends keyof M>(
    declaration: ServiceDeclaration<M[K]>,
  ): Promise<M[K]> {
    return declaration.service;
  }

  private async factory<K extends keyof M>(
    serviceName: K,
    declaration: FactoryDeclaration<M, K>,
  ): Promise<M[K]> {
    const isSingleton =
      declaration.isSingleton === undefined ? true : declaration.isSingleton;
    if (isSingleton) {
      type Service = Promise<M[K]> | M[K] | undefined;
      let service: Service = this.singletons[serviceName];
      if (service === undefined) {
        service = declaration.factory(this);
        this.singletons[serviceName] = service;
      }
      if (service === undefined) {
        throw new Error(
          `Can't find service with name "${serviceName}". Please, check the service factory: maybe it returns nothing.`,
        );
      }
      return service;
    }
    return declaration.factory(this);
  }
}

import {
  Declarations,
  DECLARATION_TYPE,
  ServiceManager,
  ServiceDeclaration,
} from './index';

describe('Service', () => {
  interface ServiceMap {
    serviceOne: object;
    serviceTwo: object;
  }
  const serviceOne = {type: 'serviceOne'};
  const serviceOneName = 'serviceOne';
  const serviceTwo = {type: 'serviceTwo'};
  const serviceTwoName = 'serviceTwo';
  const declarations: Declarations<ServiceMap> = {
    [serviceOneName]: {
      type: DECLARATION_TYPE.SERVICE,
      service: serviceOne,
    },
    [serviceTwoName]: {
      type: DECLARATION_TYPE.SERVICE,
      service: Promise.resolve(serviceTwo),
    },
  };

  let serviceManager: ServiceManager<ServiceMap>;
  beforeEach(() => {
    serviceManager = new ServiceManager<ServiceMap>(declarations);
  });

  test('Should get service one', async () => {
    await expect(serviceManager.get(serviceOneName)).resolves.toBe(serviceOne);
  });

  test('Should get service two', async () => {
    await expect(serviceManager.get(serviceTwoName)).resolves.toBe(serviceTwo);
  });

  test('Should throw error when try get undeclared service', async () => {
    await expect(
      serviceManager.get('undeclaredService' as any),
    ).rejects.toThrow('Service with name "undeclaredService" is undeclared.');
  });

  test('Should throw error when try get service with invalid declaration', async () => {
    const serviceManager = new ServiceManager({
      serviceOne: {
        type: 'invalidType' as DECLARATION_TYPE,
      } as ServiceDeclaration<any>,
    });
    await expect(serviceManager.get('serviceOne')).rejects.toThrow(
      'Invalid declaration type: "invalidType".',
    );
  });

  describe('getMap', () => {
    test('Should get service map', async () => {
      await expect(
        serviceManager.getMap({
          one: serviceOneName,
          two: serviceTwoName,
        }),
      ).resolves.toStrictEqual({
        one: serviceOne,
        two: serviceTwo,
      });
    });
  });
});

describe('Factory', () => {
  enum SERVICE {
    ONE = 'one',
    TWO = 'two',
    THREE = 'three',
    FOUR = 'four',
    FIVE = 'five',
    SIX = 'six',
    SEVEN = 'seven',
  }
  interface ServiceMap {
    [SERVICE.ONE]: object;
    [SERVICE.TWO]: object;
    [SERVICE.THREE]: Promise<'someString'>;
    [SERVICE.FOUR]: {
      dependency: 'someString';
    };
    [SERVICE.FIVE]: object;
    [SERVICE.SIX]: object;
    [SERVICE.SEVEN]: undefined;
  }
  const serviceOne = {type: 'serviceOne'};
  const serviceTwo = {type: 'serviceTwo'};
  const serviceThree = 'someString';
  const declarations: Declarations<ServiceMap> = {
    [SERVICE.ONE]: {
      type: DECLARATION_TYPE.FACTORY,
      factory: () => serviceOne,
    },
    [SERVICE.TWO]: {
      type: DECLARATION_TYPE.FACTORY,
      factory: async () => serviceTwo,
    },
    [SERVICE.THREE]: {
      type: DECLARATION_TYPE.SERVICE,
      service: Promise.resolve<'someString'>(serviceThree),
    },
    [SERVICE.FOUR]: {
      type: DECLARATION_TYPE.FACTORY,
      factory: async (serviceManager) => {
        const dependency = await serviceManager.get(SERVICE.THREE);
        return {
          dependency,
        };
      },
    },
    [SERVICE.FIVE]: {
      type: DECLARATION_TYPE.FACTORY,
      factory: () => ({type: 'serviceFive'}),
      isSingleton: true,
    },
    [SERVICE.SIX]: {
      type: DECLARATION_TYPE.FACTORY,
      factory: () => ({type: 'serviceSix'}),
      isSingleton: false,
    },
    [SERVICE.SEVEN]: {
      type: DECLARATION_TYPE.FACTORY,
      factory: () => undefined,
    },
  };

  let serviceManager: ServiceManager<ServiceMap>;
  beforeEach(() => {
    serviceManager = new ServiceManager<ServiceMap>(declarations);
  });

  test('Should get service which created by factory', async () => {
    await expect(serviceManager.get(SERVICE.ONE)).resolves.toBe(serviceOne);
  });

  test('Should get service which created by async factory', async () => {
    await expect(serviceManager.get(SERVICE.TWO)).resolves.toBe(serviceTwo);
  });

  test('Should get service which has other service as dependency', async () => {
    await expect(serviceManager.get(SERVICE.FOUR)).resolves.toStrictEqual({
      dependency: serviceThree,
    });
  });

  test('Should get same service each time by default', async () => {
    const firstValue = await serviceManager.get(SERVICE.FOUR);
    const secondValue = await serviceManager.get(SERVICE.FOUR);
    await expect(firstValue).toBe(secondValue);
  });

  test('Should get same service each time if enabled isSingleton flag', async () => {
    const firstValue = await serviceManager.get(SERVICE.FIVE);
    const secondValue = await serviceManager.get(SERVICE.FIVE);
    await expect(firstValue).toBe(secondValue);
  });

  test('Should get same instance of singleton service if request sends is two times', async () => {
    const [firstValue, secondValue] = await Promise.all([
      serviceManager.get(SERVICE.FIVE),
      serviceManager.get(SERVICE.FIVE),
    ]);
    await expect(firstValue).toBe(secondValue);
  });

  test('Should get new service each time if disabled isSingleton flag', async () => {
    const firstValue = await serviceManager.get(SERVICE.SIX);
    const secondValue = await serviceManager.get(SERVICE.SIX);
    await expect(firstValue).not.toBe(secondValue);
  });

  test('Should throw error, when service factory returns undefined', async () => {
    await expect(serviceManager.get(SERVICE.SEVEN)).rejects.toThrow(
      'Can\'t find service with name "seven". Please, check the service factory: maybe it returns nothing.',
    );
  });

  describe('getMap', () => {
    test('Should get the same instance of service for each alias in map', async () => {
      const {first, second} = await serviceManager.getMap({
        first: SERVICE.FIVE,
        second: SERVICE.FIVE,
      });
      expect(first).toBe(second);
    });

    test('Should get two new service instances if disabled is isSingleton flag', async () => {
      const {first, second} = await serviceManager.getMap({
        first: SERVICE.SIX,
        second: SERVICE.SIX,
      });
      expect(first).not.toBe(second);
    });
  });
});

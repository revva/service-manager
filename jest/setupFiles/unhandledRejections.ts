if (!process.env.LISTENING_TO_UNHANDLED_REJECTION) {
  process.on('unhandledRejection', (reason) => {
    console.error('UNHANDLED REJECTION', reason);
  });
  process.env.LISTENING_TO_UNHANDLED_REJECTION = '1';
}

# Simple inversion of control for JavaScript and Node.js

## Installation

```bash
npm i --save --save-exact @revva/service-manager
```

## Usage

```ts
import {ServiceManager, DECLARATION_TYPE} from '@revva/service-manager';

class ServiceOne {
    constructor(dependency) {
        this.dependency = dependency;
    }

    run() {
        console.log(this.dependency.getMessage());
    }
}

class ServiceTwo {
    constructor(message) {
        this.message = message;
    }

    getMessage() {
        return this.message;
    }
}

const declarations = {
    serviceOne: {
        type: DECLARATION_TYPE.FACTORY,
        factory: async (sm) => {
            return new ServiceOne(await sm.get('serviceTwo'));
        },
    },
    serviceTwo: {
        type: DECLARATION_TYPE.FACTORY,
        factory: async (sm) => {
            return new ServiceTwo(await sm.get('serviceThree'));
        },
    },
    serviceThree: {
        type: DECLARATION_TYPE.SERVICE,
        service: 'Hello world!',
    },
};
const serviceManager = new ServiceManager(declarations);
const serviceOne = serviceManager.get('serviceOne');
serviceOne.run(); // 'Hello world!'
```
